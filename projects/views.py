from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    template_name = "projects/list.html"
    model = Project

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    template_name = "projects/detail.html"
    model = Project
    context_object_name = "projects"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    template_name = "projects/create.html"
    model = Project
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
