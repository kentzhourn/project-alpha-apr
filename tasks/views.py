from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView

from tasks.models import Task

# Create your views here.
class TaskCreateView(LoginRequiredMixin, CreateView):
    template_name = "tasks/create.html"
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class TaskListView(LoginRequiredMixin, ListView):
    template_name = "tasks/list.html"
    model = Task


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "tasks/edit.html"
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
